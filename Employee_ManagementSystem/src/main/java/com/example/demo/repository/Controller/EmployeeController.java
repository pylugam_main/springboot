package com.example.demo.repository.Controller;

import java.util.List;

//import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.model.EmployeeModel;
import com.example.demo.repository.EmployeeRepository;

@Controller
public class EmployeeController {
	
	@Autowired
 private EmployeeRepository eRepo ;


	
	@GetMapping({"/list","/"})
	public ModelAndView showEmployees() {
		ModelAndView mav = new ModelAndView("list_employees");
		    List<EmployeeModel> list =  eRepo.findAll();
	          mav.addObject("employees",list);
	           return mav ;
	}
	
	
	@GetMapping("/addEmployeeform")
	public  ModelAndView addEmployeeform() {
		ModelAndView mav = new ModelAndView("add_employee_form");
		EmployeeModel newEmployee = new EmployeeModel();
		mav.addObject("employee",newEmployee);
		return mav ;
		
	}
	
	@PostMapping("/saveEmployee")
	public String saveEmloyee(@ModelAttribute EmployeeModel employee) {
		eRepo.save(employee);
		return "redirect:/list";	
	}
	
	
	  @GetMapping("/showUpdateForm")
	public ModelAndView showUpadateform(@RequestParam long employeeId) {
		ModelAndView mav = new ModelAndView("add_employee_form");
		EmployeeModel employee = eRepo.findById(employeeId).get();
		mav.addObject("employee",employee);
		return mav ;
	}
	
	@GetMapping("/deleteEmployee")
	public String deleteEmployee(@RequestParam long employeeId) {
		eRepo.deleteById(employeeId);
		return "redirect:/list";
		
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
