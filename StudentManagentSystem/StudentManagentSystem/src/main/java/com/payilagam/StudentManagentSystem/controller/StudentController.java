package com.payilagam.StudentManagentSystem.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.payilagam.StudentManagentSystem.model.StudentModel;
import com.payilagam.StudentManagentSystem.repositery.StudentRepo;

@Controller
public class StudentController {
	
	@Autowired
	StudentRepo repo;
@RequestMapping("index")
	public ModelAndView viewStud(){
	ModelAndView mav = new ModelAndView("liststudents");
	mav.addObject("studentObj",repo.findAll());
		return mav;
}	
		
		@RequestMapping("student/new")
		public ModelAndView  createStudent(StudentModel stud) {
	
			ModelAndView mav = new ModelAndView("createStudent");
			mav.addObject("studobj",stud);
			return mav;
		}   
		
		@PostMapping("Student/new")
		public String saveStudent(StudentModel stud) {
			
			repo.save(stud);
			return "redirect:/index";
		}
		
			@RequestMapping("student/edit/{id}")
			public ModelAndView editStudent(@PathVariable int id) {
				System.out.println("222222222222222222222222222");
				ModelAndView mav = new ModelAndView("updateStudents");
				mav.addObject("studObj",repo.findById(id).get());
				return mav;
			}
			@PostMapping("Student/edit")
			public String updateStudent(StudentModel model) {
				repo.save(model);
				return "redirect:/index";
			}
			@RequestMapping("student/delete/{id}")
			public String deleteStudent(@PathVariable int id) {
				repo.deleteById(id);
				return "redirect:/index";
				
			}
			
			
			
}
