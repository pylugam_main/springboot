package com.payilagam.StudentManagentSystem;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StudentManagentSystemApplication {

	public static void main(String[] args) {                                      
		SpringApplication.run(StudentManagentSystemApplication.class, args);
	}

}
