package com.payilagam.StudentManagentSystem.repositery;

import org.springframework.data.jpa.repository.JpaRepository;

import com.payilagam.StudentManagentSystem.model.StudentModel;

public interface StudentRepo extends JpaRepository<StudentModel, Integer> {

}
