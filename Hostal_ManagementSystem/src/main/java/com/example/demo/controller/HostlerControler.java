package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.Repository.Hostal_repo;
import com.example.demo.model.HostlarModel;

@Controller
public class HostlerControler {

	@Autowired
private Hostal_repo hRepo ;
	
	
	@GetMapping("/list")
public ModelAndView showHostler() {	
ModelAndView mav = new ModelAndView("list_hostler");
List<HostlarModel> list = hRepo.findAll();
mav.addObject("hostlers",list);
  return mav ;	
}	
	
	@GetMapping("/addHostlerform")
public ModelAndView addHosteler() {
	ModelAndView mav = new ModelAndView("add_hostler");
	HostlarModel newhosteler = new HostlarModel();
	mav.addObject("hostlers",newhosteler);
	return mav ;
	
}

	@PostMapping("/saveHostler")
public String savehostler(@ModelAttribute HostlarModel hostler) {
	hRepo.save(hostler);
	return "redirect:list";
}
	
	
	@GetMapping("/showUpdateform")
public ModelAndView showHostlerform(@RequestParam long hostlerId ) {
	ModelAndView mav = new ModelAndView("add_hostler");
	HostlarModel hostler = hRepo.findById(hostlerId).get();
	mav.addObject("hostlers",hostler);
	return mav ;
}
	
	
	@GetMapping("/deleteHostler")
public String deleteHostler(@RequestParam long hostlerId) {
	hRepo.deleteById(hostlerId);
	return "redirect:/list";		
}

	
}
