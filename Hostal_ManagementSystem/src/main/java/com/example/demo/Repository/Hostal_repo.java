package com.example.demo.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.model.HostlarModel;

public interface Hostal_repo extends JpaRepository<HostlarModel, Long> {

}
